'use strict';

/**
 * @ngdoc function
 * @name capsotestApp.controller:CompanyCtrl
 * @description
 * # CompanyCtrl
 * Controller of the capsotestApp
 */

(function () {
    angular
        .module('capsotestApp')
        .controller('CompanyCtrl', CompanyCtrl);

    CompanyCtrl.$inject = ['$scope', '$q', '$http', '$state', 'apiUrl', '$uibModal'];

    function CompanyCtrl($scope, $q, $http, $state, apiUrl, $uibModal) {

        var vm = this;
        vm.companies = {};
        vm.bonds = [];
        vm.companyName = '';
        vm.showDiv = false;
        vm.showProgress = false;


        // interface
        vm.activate = activate;
        vm.getCompanies = getCompanies;
        vm.showBondInfo = showBondInfo;

        vm.activate();

        // functions
        function activate() {
            vm.getCompanies();
        }


        function getCompanies() {
            $http.get(apiUrl + 'companies.json').
                success(function (data, status, headers, config) {
                    vm.companies = data.results;
                console.log(data.results);
                }).
                error(function (data, status, headers, config) {
                    console.log(status);
                });

        }

        function showBondInfo(id, name) {
            var bond = {};
            vm.bonds = [];
            vm.showProgress = true;
            vm.companyName = name;
            $http.get(apiUrl + 'bondmaster.json').
                success(function (data, status, headers, config) {
                 vm.showDiv = true;
                 vm.showProgress = false;
                        angular.forEach(data, function (value, key) {
                            if (id == value.issuerId) {
                                bond = {
                                    cusip: value.cusip,
                                    price: parseFloat(value.prices[key].price),
                                    couponRate: value.couponRate
                                };

                            vm.bonds.push(bond);
                            }


                 });
            });
            return vm.bonds;
        }

    }
})();