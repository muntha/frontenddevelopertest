'use strict';

/**
 * @ngdoc overview
 * @name capsotestApp
 * @description
 * # capsotestApp
 *
 * Main module of the application.
 */
angular.module('capsotestApp', [
	'ui.router',
	'ngAnimate',
	'ngResource',
	'ngSanitize',
    'ui.bootstrap',
	'ngTouch'
])
.constant('apiUrl', 'https://s3-eu-west-1.amazonaws.com/capso.test.data/')
.config(function($stateProvider, $urlRouterProvider, $resourceProvider) {

	$urlRouterProvider.otherwise('/');
    $resourceProvider.defaults.stripTrailingSlashes = false;
	$stateProvider
		.state('main', {
			url: '/',
			templateUrl: 'views/main.html',
			controller: 'MainCtrl as mainCtrl'
		})
        .state('companies', {
			url: '/',
			templateUrl: 'views/companies.html',
			controller: 'CompanyCtrl as companyCtrl'
		});
});
